﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco
{
    class ContaCorrente
    {
        public int agencia = 3032;
        public int conta = 123456;
        public float valor = 0;
        public String nomeProprietario = "Mustafar";
        public DateTime horario;
        int moeda;
        Saldo meuSaldo = new Saldo();

        public DateTime Horario
        {
            get
            {
                return horario;
            }
        }

        public float Valor
        {
            get
            {
                return valor;
            }
            set
            {
                valor = value;
            }
        }
        public int Agencia
        {
            get
            {
                return agencia;
            }
        }
        public int Conta
        {
            get
            {
                return conta;
            }
        }

        public String NomeProprietario
        {
            get
            {
                return nomeProprietario;
            }
        }

        public void ChecarSaldo()
        {
            Console.WriteLine("Seu saldo em Real é: " + meuSaldo.QuantiaReal + ", e seu saldo em Dollar é: " 
                + meuSaldo.QuantiaDollar);
        }

        public void Deposito(int Agencia, int Conta)
        {
            Console.WriteLine("Entre com o numero da agencia: ");
            Agencia = int.Parse(Console.ReadLine());

            Console.WriteLine("Entre com a conta: ");
            Conta = int.Parse(Console.ReadLine());

            Console.WriteLine("Entre com a moeda e valor desejados: ");

            moeda = int.Parse(Console.ReadLine());

            if (moeda == 1)
            {
                Console.WriteLine("Você escolheu o Real. Agora entre com o valor: ");
                Valor = float.Parse(Console.ReadLine());
                meuSaldo.QuantiaReal = Valor + meuSaldo.QuantiaReal;
                horario = DateTime.Now;
         
            }
            
            if (moeda == 2)
            {
                Console.WriteLine("Você escolheu o Dollar. Agora entre com o valor: ");
                Valor = float.Parse(Console.ReadLine());
                meuSaldo.QuantiaDollar = Valor + meuSaldo.QuantiaDollar;
                horario = DateTime.Now;
            }            
        }
    }
}
