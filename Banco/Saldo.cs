﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco
{
    public class Saldo
    {
        private static float quantiaReal;
        private static float quantiaDollar;
        private String moeda;

        public float QuantiaReal
        {
            get
            {
                return quantiaReal;
            }

            set
            {
                quantiaReal = value;
            }
        }

        public float QuantiaDollar
        {
            get
            {
                return quantiaDollar;
            }

            set
            {
                quantiaDollar = value;
            }
        }
    }
}
