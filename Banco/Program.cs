﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco
{
    class Program
    {
        static void Main(string[] args)
        {
            ContaCorrente minhaConta = new ContaCorrente();
            int agencia = 0;
            int conta = 0;
            bool valida = false;
            int nD = 0;
            int nS = 0;
            int opcao = 0;
            List<float> historico = new List<float>();
            List<float> historicoValores = new List<float>();
            var meuHistorico = new Historico<string, float> ();
            meuHistorico.Add("Número do depósito", new List<float>());
            meuHistorico.Add("Valor do depósito", new List<float>());



            while (valida == false)
            {
                Console.WriteLine("Entre com a opção desejada: ");
                opcao = int.Parse(Console.ReadLine());

                switch (opcao)
                {
                    case 1:
                        Console.WriteLine("Você escolheu Checar Saldo. Entre com sua agência e conta: ");
                        agencia = int.Parse(Console.ReadLine());
                        conta = int.Parse(Console.ReadLine());
                        if (agencia == minhaConta.Agencia && conta == minhaConta.Conta)
                        {
                            valida = true;
                            Console.WriteLine("Seja bem vindo " + minhaConta.NomeProprietario);
                            minhaConta.ChecarSaldo();
                        }
                        else
                        {
                            Console.WriteLine("Agência ou conta incorretos. Tecle 1 para tentar novamente ou tecle 4 para sair.");
                            valida = false;
                        }
                        break;

                    case 2:
                        Console.WriteLine("Bem vindo ao depósito. Digite a agência e contas a serem creditadas: ");
                        minhaConta.Deposito(agencia, conta);
                        nD++;
                        meuHistorico["Número do depósito"].Add(nD);
                        meuHistorico["Valor do depósito"].Add(minhaConta.Valor);
                        //historico.Add(nD);
                        //historicoValores.Add(minhaConta.Valor);
                        //historico.Add(minhaConta.Horario);

                        historicoValores.Add(minhaConta.Valor);
                        Console.WriteLine("Se deseja realizar outro depósito, digite 5");
                        opcao = int.Parse(Console.ReadLine());
                        if (opcao == 5)
                            valida = false;
                        else
                            opcao = int.Parse(Console.ReadLine());
                            //valida = true;
                        break;

                    case 3:
                        Console.WriteLine("Você selecionou Extrato. Estas foram as operações realizadas: ");
                        foreach(float numero in meuHistorico["Número do depósito"])
                        {
                            Console.Write("Número do depósito: " + numero + ", ");
                            foreach (float valor in meuHistorico["Valor do depósito"])
                            {
                                Console.WriteLine("Valor do depósito" + valor);
                            }
                            Console.WriteLine();
                        }
                       
                        break;

                    case 4:
                        Console.WriteLine("Você escolheu sair. Adeus.");
                        valida = true;
                        break;
                }
            }

            minhaConta.ChecarSaldo();
        }
    }
}
